-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 05 avr. 2020 à 17:19
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gsb`
--

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

DROP TABLE IF EXISTS `etat`;
CREATE TABLE IF NOT EXISTS `etat` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `libelle` text CHARACTER SET utf8 NOT NULL,
  `ordre` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `fiche`
--

DROP TABLE IF EXISTS `fiche`;
CREATE TABLE IF NOT EXISTS `fiche` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `mois` int(2) NOT NULL,
  `annee` int(4) NOT NULL,
  `montant_validite` int(255) NOT NULL,
  `etat_id` int(11) NOT NULL,
  `utlisateur_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ligneff`
--

DROP TABLE IF EXISTS `ligneff`;
CREATE TABLE IF NOT EXISTS `ligneff` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `dateff` date DEFAULT NULL,
  `typeff_id` int(11) DEFAULT NULL,
  `qte` int(255) DEFAULT NULL,
  `etat_id` int(11) DEFAULT NULL,
  `fiche_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ligneff`
--

INSERT INTO `ligneff` (`id`, `dateff`, `typeff_id`, `qte`, `etat_id`, `fiche_id`) VALUES
(1, '2020-04-03', 2, 1, NULL, NULL),
(47, '2020-04-19', 2, 9, NULL, NULL),
(48, '2020-05-07', 1, 2, NULL, NULL),
(49, '2020-05-15', 1, 4, NULL, NULL),
(50, '2020-03-12', 2, 2, NULL, NULL),
(51, '2020-03-18', 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `lignehf`
--

DROP TABLE IF EXISTS `lignehf`;
CREATE TABLE IF NOT EXISTS `lignehf` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `dateHF` date DEFAULT NULL,
  `libelle` text CHARACTER SET utf8 NOT NULL,
  `montant` int(255) NOT NULL,
  `fiche_id` int(11) DEFAULT NULL,
  `etat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lignehf`
--

INSERT INTO `lignehf` (`id`, `dateHF`, `libelle`, `montant`, `fiche_id`, `etat_id`) VALUES
(15, '2020-04-09', 'ggrgr', 4, NULL, NULL),
(16, '2020-03-13', 'bite', 12, NULL, NULL),
(17, '2020-05-17', 'tamerelapute', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `typeff`
--

DROP TABLE IF EXISTS `typeff`;
CREATE TABLE IF NOT EXISTS `typeff` (
  `idtypeff` int(255) NOT NULL AUTO_INCREMENT,
  `libelle` text CHARACTER SET utf8 NOT NULL,
  `montant_unitaire` int(255) NOT NULL,
  PRIMARY KEY (`idtypeff`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typeff`
--

INSERT INTO `typeff` (`idtypeff`, `libelle`, `montant_unitaire`) VALUES
(1, 'repas-midi', 20),
(2, 'nuitée', 80);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `nom_utilisateur` varchar(100) NOT NULL,
  `mot_de_passe` varchar(100) NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `adresse` text CHARACTER SET utf8,
  `ville` text CHARACTER SET utf8,
  `codepost` int(255) DEFAULT NULL,
  `poste` varchar(255) CHARACTER SET utf8 NOT NULL,
  `dateInscription` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom_utilisateur`, `mot_de_passe`, `nom`, `prenom`, `email`, `adresse`, `ville`, `codepost`, `poste`, `dateInscription`) VALUES
(20, 'adminvisite', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', 'vivie', 'jeremy', 'jeremyvivie@hotmail.fr', 'rue du poulpe', 'lyon', 83470, 'Visiteur', '2020-03-09'),
(21, 'admincompta', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', 'vivie', 'jeremy', 'jeremyvivie@hotmail.fr', 'rue du poulpe', 'lyon', 83470, 'Comptable', '2020-03-09'),
(23, 'admin', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', 'ViviÃ©', 'JÃ©rÃ©my', 'jeremyvivie@hotmail.fr', 'rue du poulpe', 'lyon', 83470, 'Administration', '2020-03-11'),
(24, 'test', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', 'Vivié', 'Jérémy', 'jeremyvivie@hotmail.fr', 'rue du poulpe', 'lyon', 83470, 'Comptable', '2020-03-11'),
(25, 'yannou', '00d70c561892a94980befd12a400e26aeb4b8599', 'yannnnn', 'yannnsss', 'yannnb@gm.com', '20', 'Lille', 1001, 'Visiteur', '2020-04-05');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
