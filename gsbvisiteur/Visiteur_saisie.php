<?php
session_start();
include('../gsblogin/connexion_bdd.php');

 // include('../connexion_bdd.php');
// test si la valeur session login est existante
// si non : renvoie sur la page de 
if(!isset($_SESSION['id'])){
  header('location: gsb/gsblogin/login.php');
}else{
    if($_SESSION['poste'] != 'Visiteur'){
      session_destroy();
      header('location: ../login.php');
    }
    $idVisiteur = $_SESSION["id"];
    $visiteur = $bdd->query("SELECT * FROM utilisateurs WHERE id='$idVisiteur'");
    $visiteurData = $visiteur->fetch();
 
    $nom=$visiteurData['nom'];
    $prenom=$visiteurData['prenom'];

    //éfinir la fiche du mois concernée en se basna t sur la date du jour
    $jour = date('d');
    $moisFiche = date('m');
    $anneeFiche = date('Y');

    if($jour>10){
      $moisFiche++;
      if($moisFiche>12){
        $moisFiche = 1;
        $anneeFiche++;
      }
    }

    //vérifier si le visteur possède déjà une fiche pour ce mois/année
    





    
    $fiche = $bdd->query("SELECT * FROM fiche WHERE utilisateur_id='$idVisiteur' AND mois = '$moisFiche' AND annee = '$anneeFiche'");
    $ficheData = $fiche->fetch();
    $fiche_id = null;
    if($fiche->rowCount() == 1){
      $fiche_id = $ficheData['id'];

    }else{
      
      $req = $bdd->prepare('INSERT INTO fiche(mois, annee, utilisateur_id, etat_id) VALUES(:mois, :annee, :utilisateur_id, :etat_id)');
      
      $req->execute(array(
        
        'mois' => $moisFiche,
        'annee' => $anneeFiche,
        'utilisateur_id' => $idVisiteur,
        'etat_id' => 1
        
      ));
        var_dump($req);



    }

}
?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Saisie des frais</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="stylesheet" href="saisiefrais.css">
    


</head>
<body>
    <div>
      
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <img src="img/p8.png">   
          <a class="navbar-brand" >Bonjour <?php echo $prenom.' '.$nom ?></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
        
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="#">Saisie de frais<span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="visiteur_consultation.php">Consulter ses frais</a>
              </li>
              <li class="nav-item dropdown">
              </li>
              
            </ul>
          </div>
        </nav>
      </div>

    <h1 style="text-align: center;">SAISIE DESde La Fiche <?php echo $moisFiche.'/'.$anneeFiche;?></h1>




    <div class="ZoneSaisie">


        <h2>Frais forfait</h2>
        <div class="Zone-frais-forfait">
            <form method="POST" action="visiteur_saisie.php" >
            <br>
                <label for="">date du frais : </label>
                <input type="date" name="date_frais" id="date_frais"> <br>
                <label for="type-frais">type du frais : </label>
                <select name="type_frais" placeholder="type de frais"> <br>
                <?php
                 $lestypesff = $bdd->query('SELECT *  FROM typeff ');   
            
                 while ($donnees = $lestypesff->fetch())
                 {
                ?>
                  <option value='<?php echo $donnees["idtypeff"]; ?>' name="idtf"><?php echo $donnees["libelle"]; ?></option>
                <?php
                }
                ?>
                </select>
                <br>
                <label for="">Quantité : </label>
                <input type="number" name="quantite" id="qte"> <br>
            
                <input type="submit" name="submitFF"  value="Envoyé">
            </form>
        </div>

        <?php
        

       
       
if (isset($_POST['submitFF'])){

  $date_frais = $_POST['date_frais'];

   //* permet de comparer si la date du jour avec la date de la ligne frais saisie
   if($date_frais <= date("Y-m-d") and strtotime($date_frais) >= (strtotime(date('Y-m-d') ."-365 days" ) ) ) {  
    
    
    $type_frais = $_POST['type_frais'];
    $quantite = $_POST['quantite'];
    
        $req = $bdd->prepare('INSERT INTO ligneff(dateff, typeff_id, qte, etat_id, fiche_id) VALUES(:dateff, :typeff_id, :qte, :etat_id, :fiche_id)');
        $req->execute(array(
          'dateff' => $date_frais,
          'typeff_id' => $type_frais,
          'qte' => $quantite,
          'etat_id' => 1,
          'fiche_id' => $fiche_id
          ));
          echo 'le frais a bien été ajouté !';
          header('location: Visiteur_saisie.php' );
          
        }else{
          echo 'la date du frais ne peux pas être supérieur à la date du jours et inférieur à 1an.';
        }
      }
?>

        <h2>Frais Hors forfait</h2>
        <div class="Zone-frais-hors-forfait">
            <form method="POST" action="visiteur_saisie.php" >
                <br>
                <label for="">date du frais : </label>
                <input type="date" name="date_frais" id="date_frais"> <br>
                <label for="">libellé du frais : </label>
                <input type="text" name="libelleHF" id="libelleHF"><br>
                <label for="">Montant du frais : </label>
                <input type="number" name="montantHF" id="montantHF"><br>
                
                <input type="submit" name="submitHF" value="Envoyé">
            </form>   
        </div>

        <?php
if (isset($_POST['submitHF'])){
  $date_frais = $_POST['date_frais'];
  if($date_frais <= date("Y-m-d") and strtotime($date_frais) >= (strtotime(date('Y-m-d') ."-365 days" ) ) ) {

$libelleHF = $_POST['libelleHF'];
$montantHF = $_POST['montantHF'];
$req = $bdd->prepare('INSERT INTO lignehf(dateHF, libelle, montant, etat_id, fiche_id) VALUES(:dateHF, :libelle, :montant, :etat_id, :fiche_id)');
$req->execute(array(
	'dateHF' => $date_frais,
  'libelle' => $libelleHF,
  'montant' => $montantHF,
  'etat_id' => 1,
  'fiche_id' => $fiche_id
	));
  header('Location: Visiteur_saisie.php' );
echo 'le frais a bien été ajouté !';

  }else{
  echo 'la date du frais ne peux pas être supérieur à la date du jours et inférieur à 1an.';
  }
}
?>
    
    </div>







    
    <script src="anim.js"></script>
</body>
</html>