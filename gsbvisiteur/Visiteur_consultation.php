<?php
session_start();
include('../gsblogin/connexion_bdd.php');
// test si la valeur session login est existante
// si non : renvoie sur la page de 
if(!isset($_SESSION['id'])){
  header('location: ../login.php');
}else{
    if($_SESSION['poste'] != 'Visiteur'){
      session_destroy();
      header('location: ../login.php');
    }
    $idVisiteur = $_SESSION["id"];
    $visiteur = $bdd->query("SELECT * FROM utilisateurs WHERE id='$idVisiteur'");
    $visiteurData = $visiteur->fetch();
 
    $nom=$visiteurData['nom'];
    $prenom=$visiteurData['prenom'];
}
?>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <title>Consultation des frais</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="Consult.css">



</head>

<body style="text-align:center">
  <div>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <img src="img/p8.png">
      <a class="navbar-brand">Bonjour <?php echo $prenom ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Consulter ses frais<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Visiteur_saisie.php">Saisir ses frais</a>

        </ul>
      </div>
    </nav>
  </div>




  <?php
try
{
	// On se connecte à MySQL
	$bdd = new PDO('mysql:host=localhost;dbname=gsb;charset=utf8', 'root', '');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}
$idmois = 0;
?>

<br>
<br>
<form method="GET">
  <label for="type-mois">Choix du mois : </label>
  <select name="fiche_id" placeholder="choisir le mois"> <br>
    <?php
                 $reponse = $bdd->query("SELECT *  FROM fiche WHERE utilisateur_id = $idVisiteur");   
            
                 while ($donnees = $reponse->fetch())
                 {
                ?>
    <option value='<?php echo $donnees["id"]; ?>' name="idmois"><?php echo $donnees["mois"], '-',  $donnees['annee']; ?></option>
    <?php
                }
                ?>
  </select>
  <input type="submit" value="submit">
</form>
  <div class="shadow p-3 mb-5 " Regular shadow style="margin: 8%;">

<?php
    if(isset($_GET['fiche_id'])){
        $fiche_id = $_GET['fiche_id'];
        
        $reponse = $bdd->query("SELECT * FROM fiche  WHERE id = '$fiche_id' ");               
        $donnees = $reponse->fetch();
        $idmois = $donnees['mois'];
        $anneeFiche = $donnees['annee'];
  $moisenlettre =" ";
    if($idmois == 0){
      $moisenlettre = ' ';
    }elseif($idmois == 1){
      $moisenlettre = 'Janvier';
    }elseif($idmois ==2){
      $moisenlettre = 'Fevrier';
    }elseif($idmois ==3){
      $moisenlettre = 'Mars';
    }elseif($idmois ==4){
      $moisenlettre = 'Avril';
    }elseif($idmois ==5){
      $moisenlettre = 'Mai';
    }elseif($idmois ==6){
      $moisenlettre = 'Juin';
    }elseif($idmois ==7){
      $moisenlettre = 'Juillet';
    }elseif($idmois ==8){
      $moisenlettre = 'Aout';
    }elseif($idmois ==9){
      $moisenlettre = 'Septembre';
    }elseif($idmois ==10){
      $moisenlettre = 'Octobre';
    }elseif($idmois ==11){
      $moisenlettre = 'Novembre';
    }elseif($idmois ==12){
      $moisenlettre = 'Décembre';
    }
    
    echo "<h2>Vos frais forfaité du mois de $moisenlettre / $anneeFiche :  </h2>";
    ?>
    <table class="table table-striped table-dark table-responsive-md">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Date</th>
          <th scope="col">Type du frais</th>
          <th scope="col">Montant</th>
        </tr>
      </thead>

  <?php

          $reponse = $bdd->query("SELECT * FROM ligneff INNER JOIN typeff ON typeff.idtypeff = ligneff.typeff_id WHERE fiche_id = '$fiche_id' ");   
            
          while ($donnees = $reponse->fetch())
            {


      ?>
      <tbody>
        <tr class="FichesFrais" id="Fiche1">
          <th scope="row"> <?php echo $donnees['id'] ?></th>
          <td> <?php echo $donnees['dateff'] ?></td>
          <td> <?php echo $donnees['libelle'] ?></td>
          <td> <?php echo $donnees['montant_unitaire']*$donnees['qte'] ?></td> 
        </tr>
      </tbody>
      <?php 
            }
          
      $reponse->closeCursor(); // Termine le traitement de la requête ?>


    </table>
    <table class="table table-striped table-dark table-responsive-md">
      <?php echo '<h2>Vos frais non forfaité du mois de ', $moisenlettre, ': </h2>'; ?>
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Date</th>
          <th scope="col">libellé</th>
          <th scope="col">Montant</th>
        </tr>
      </thead>
      
      <?php
      
            $reponse = $bdd->query("SELECT *  FROM lignehf WHERE fiche_id = '$fiche_id'");   
            
            while ($donnees = $reponse->fetch())
            {

      ?>
      <tbody>
        <tr class="FichesFrais" id="Fiche1">
          <th scope="row"> <?php echo $donnees['id'] ?></th>
          <td> <?php echo $donnees['dateHF'] ?></td>
          <td> <?php echo $donnees['libelle'] ?></td>
          <td> <?php echo $donnees['montant'] ?></td>
        </tr>
      </tbody>
      <?php 
            }
          }
      $reponse->closeCursor(); // Termine le traitement de la requête ?>
    </table>
  </div>





  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
  </script>
  <script src="anim.js"></script>
</body>

</html>